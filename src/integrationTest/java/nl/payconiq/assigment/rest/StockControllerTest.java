package nl.payconiq.assigment.rest;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import nl.payconiq.assigment.stock.model.Stock;
import nl.payconiq.assigment.stock.repository.StockRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StockControllerTest {

	@Autowired
	private WebApplicationContext context;
	@Autowired
	private StockRepository repository;
	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void collectStocks_Success() throws Exception {
		Stock stock = Stock.builder()
			.name("Amazon")
			.currentPrice(BigDecimal.ONE)
			.build();
		repository.save(stock);
		mvc.perform(get("/api/v1/stocks", "")).andExpect(status().isOk()).andExpect(jsonPath("$.content", is(not(empty()))));
	}

}
