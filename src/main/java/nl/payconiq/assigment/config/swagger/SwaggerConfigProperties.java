package nl.payconiq.assigment.config.swagger;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties("swagger")
class SwaggerConfigProperties {

	private String title;
	private String description;
	private Contact contact;

	@Data
	public static class Contact {
		private String name;
		private String url;
		private String email;
	}

}