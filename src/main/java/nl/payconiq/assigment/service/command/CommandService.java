package nl.payconiq.assigment.service.command;

public interface CommandService<T, D> {

	T save(T entity);

	void delete(T entity);

	T update(Long id, D dto);
}
