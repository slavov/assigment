package nl.payconiq.assigment.exception;

public class EntityNotFoundException extends RuntimeException {

	public EntityNotFoundException(final String message) {
		super(message);
	}

}
