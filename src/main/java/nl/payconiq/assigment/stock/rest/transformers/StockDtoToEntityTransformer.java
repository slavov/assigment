package nl.payconiq.assigment.stock.rest.transformers;

import java.util.function.Function;

import nl.payconiq.assigment.stock.model.Stock;
import nl.payconiq.assigment.stock.rest.dto.StockDto;

import org.springframework.stereotype.Component;

@Component
public class StockDtoToEntityTransformer implements Function<StockDto, Stock> {

	@Override
	public Stock apply(final StockDto stockDto) {
		return Stock.builder()
			.name(stockDto.getName())
			.currentPrice(stockDto.getCurrentPrice())
			.lockVersion(stockDto.getLockVersion())
			.build();
	}

}
