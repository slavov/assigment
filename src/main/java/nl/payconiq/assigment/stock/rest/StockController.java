package nl.payconiq.assigment.stock.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import nl.payconiq.assigment.service.command.CommandService;
import nl.payconiq.assigment.service.query.QueryService;
import nl.payconiq.assigment.stock.model.Stock;
import nl.payconiq.assigment.stock.rest.dto.StockDto;
import nl.payconiq.assigment.stock.rest.transformers.StockDtoToEntityTransformer;
import nl.payconiq.assigment.stock.rest.transformers.StockEntityToDtoTransformer;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(path = "api/v1/stocks", produces = APPLICATION_JSON_VALUE)
@Api(tags = "Stocks Public API")
@SwaggerDefinition(tags = { @Tag(name = "Stocks Public API", description = "Public API for stocks") })
public class StockController {

	private final CommandService<Stock, StockDto> commandService;
	private final QueryService<Stock> queryService;
	private final StockEntityToDtoTransformer stockEntityToDtoTransformer;
	private final StockDtoToEntityTransformer stockDtoToEntityTransformer;

	@GetMapping
	@ApiOperation("Find all available companies in stocks")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Results page you want to retrieve (0..N)"),
		@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Number of records per page."),
		@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
			value = "For sorting criteria is in the format: property(,asc|desc). " +
				"Default sort order is ascending " +
				"Multiple sort criteria are supported")
	})
	public ResponseEntity<Page<StockDto>> findAll(final Pageable pageable) {
		final Page<Stock> stocks = queryService.findAll(pageable);
		final List<StockDto> stockDtos = stocks.stream()
			.map(stockEntityToDtoTransformer)
			.collect(Collectors.toList());
		return ResponseEntity.ok(new PageImpl<>(stockDtos));
	}

	@GetMapping(value = "/{id}")
	@ApiOperation("Retrieve a specific company from stock by id")
	public ResponseEntity<StockDto> findOne(@PathVariable final Long id) {
		final Stock stock = queryService.collectStock(id);
		log.info("Found stock with id '{}'", stock.getId());
		final StockDto stockDto = stockEntityToDtoTransformer.apply(stock);
		return ResponseEntity.ok(stockDto);
	}

	@DeleteMapping(value = "/{id}")
	@ApiOperation("Delete a record from stock")
	public ResponseEntity delete(@PathVariable final Long id) {
		commandService.delete(queryService.collectStock(id));
		return ResponseEntity.noContent().build();
	}

	@PutMapping(value = "/{id}")
	@ApiOperation("Update a stock record")
	public ResponseEntity<StockDto> update(@PathVariable final Long id, @RequestBody @Valid final StockDto stockDto) {
		final Stock stock = queryService.collectStock(id);
		final Stock savedStock = commandService.update(stock.getId(), stockDto);
		final StockDto updatedStock = stockEntityToDtoTransformer.apply(savedStock);
		return ResponseEntity.ok(updatedStock);
	}

	@PostMapping
	@ApiOperation("Create a stock record")
	public ResponseEntity<StockDto> create(@RequestBody @Valid final StockDto stockDto, final HttpServletRequest request) {
		final Stock newStock = stockDtoToEntityTransformer.apply(stockDto);
		Stock created = commandService.save(newStock);
		return ResponseEntity.created(ServletUriComponentsBuilder.fromContextPath(request).path("{path}/{id}")
			.buildAndExpand(request.getServletPath(), created.getId()).toUri())
			.body(stockDto);
	}

}
