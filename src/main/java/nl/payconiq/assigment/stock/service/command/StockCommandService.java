package nl.payconiq.assigment.stock.service.command;

import nl.payconiq.assigment.service.command.CommandService;
import nl.payconiq.assigment.stock.model.Stock;
import nl.payconiq.assigment.stock.repository.StockRepository;
import nl.payconiq.assigment.stock.rest.dto.StockDto;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class StockCommandService implements CommandService<Stock, StockDto> {

	private final StockRepository repository;

	@Override
	public Stock save(final Stock entity) {
		return repository.save(entity);
	}

	@Override
	public void delete(final Stock entity) {
		repository.delete(entity);
	}

	@Override
	public Stock update(final Long stockId, final StockDto stockDto) {
		final Stock stock = Stock.builder()
			.id(stockId)
			.name(stockDto.getName())
			.currentPrice(stockDto.getCurrentPrice())
			.lockVersion(stockDto.getLockVersion())
			.build();
		return repository.save(stock);
	}

}
